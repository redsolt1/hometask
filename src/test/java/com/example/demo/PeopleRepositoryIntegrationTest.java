package com.example.demo;

import com.example.demo.domain.People;
import com.example.demo.repos.PeopleRepo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.testng.Assert;

import java.util.List;

import static org.testng.AssertJUnit.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PeopleRepositoryIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private PeopleRepo peopleRepo;


    private People johnDoe1 = new People("John Doe", "123-45-67");
    private People johnDoe2 = new People("John Doe", "123-45-68");
    private People johnDoe3 = new People("John Doe 3", "123-45-69");

    @Test
    public void storeAndCheckOneRecord() {
        entityManager.persist(johnDoe1);
        entityManager.flush();

        List<People> found = peopleRepo.findByName(johnDoe1.getName());
        Assert.assertEquals(found.size(), 1);

        assertEquals(found.get(0).getName(), johnDoe1.getName());
        assertEquals(found.get(0).getPhoneNumber(), johnDoe1.getPhoneNumber());
    }

    @Test
    public void storeTwoRecordAndRemoveOneRecord() {
        entityManager.persist(johnDoe1);
        entityManager.persist(johnDoe2);

        entityManager.flush();

        List<People> found = peopleRepo.findByName(johnDoe1.getName());
        Assert.assertEquals(found.size(), 2);

        entityManager.remove(johnDoe2);
        entityManager.flush();

        found = peopleRepo.findByName(johnDoe1.getName());
        Assert.assertEquals(found.size(), 1);

        assertEquals(found.get(0).getName(), johnDoe1.getName());
    }

    @Test
    public void storeThreeRecordRemoveOneSearchByNameRecord() {
        entityManager.persist(johnDoe1);
        entityManager.persist(johnDoe2);
        entityManager.persist(johnDoe3);

        entityManager.flush();

        List<People> found = (List<People>) peopleRepo.findAll();
        Assert.assertEquals(found.size(), 3);

        entityManager.remove(johnDoe2);
        entityManager.flush();

        found = peopleRepo.findByName(johnDoe1.getName());

        Assert.assertEquals(found.size(), 1);

        found = (List<People>) peopleRepo.findAll();
        Assert.assertEquals(found.size(), 2);

        assertEquals(found.get(0).getName(), johnDoe1.getName());
        assertEquals(found.get(1).getName(), johnDoe3.getName());
    }

}