package com.example.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.asserts.SoftAssert;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class DemoApplicationTests {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    private String address = "http://localhost:8080/";

    private String name1 = "John Doe 1";
    private String name2 = "John Doe 2";
    private String name3 = "John Doe 3";
    private List<String> listOfNames = new ArrayList<String>(){{
        add(name1);
        add(name2);
        add(name3);
    }};


    private String phoneNumber1 = "123-123-1231";
    private String phoneNumber2 = "123-123-1232";
    private String phoneNumber3 = "123-123-1233";
    private List<String> listOfphoneNumber = new ArrayList<String>(){{
        add(phoneNumber1);
        add(phoneNumber2);
        add(phoneNumber3);
    }};

    @Test
    public void addThreeNames() throws Exception {

        this.setup();
        addrecordMethod(name1, phoneNumber1);
        addrecordMethod(name2, phoneNumber2);
        addrecordMethod(name3, phoneNumber3);
        String responce = getMain();

        geteRecordSList(responce);

        SoftAssert asert=new SoftAssert();
        assertTrue(responce.contains(name1));
        assertTrue(responce.contains(phoneNumber1));
        assertTrue(responce.contains(name2));
        assertTrue(responce.contains(phoneNumber2));
        assertTrue(responce.contains(name3));
        assertTrue(responce.contains(phoneNumber3));
        asert.assertAll();

    }

    @Test
    public void filterByNonExistsName() throws Exception {
        String name = "testName";
        String phoneNumber = "000000000";

        this.setup();
        this
                .mockMvc
                .perform(get("/filter")
                        .param(name, phoneNumber))
                .andDo(print())
                .andExpect(status().isOk());

        String responce = getMain();
        String stringPattern = "<span>(.+)?</span>";
        List<String> list = regex(responce, stringPattern);
        assertEquals(list.size(), 0);
    }

    @Test
    public void addThreeNamesAndFilter() throws Exception {

        this.setup();
        if (geteRecordSList(getMain()) == 3) {

            this.setup();
            String afterFiltering =
                    this
                            .mockMvc
                            .perform(post("/filter")
                                    .param("filter", name2))
                            .andDo(print())
                            .andExpect(status().isOk())
                            .andReturn()
                            .getResponse()
                            .getContentAsString();

            String stringPattern = "<span>(.+)?</span>";
            List<String> list = regex(afterFiltering, stringPattern);
            assertEquals(list.size(), 1);
            assertTrue(list.get(0).equals(name2));
            stringPattern = "<i>(.+)?</i>";
            list = regex(afterFiltering, stringPattern);
            assertTrue(list.get(0).equals(phoneNumber2));
        };
    }


    private void addrecordMethod(String name, String phoneNumber) throws Exception {
        this
                .mockMvc
                .perform(post("/addrecord")
                        .param("name", name)
                        .param("phoneNumber", phoneNumber))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(mvcResult -> mvcResult.getResponse().getContentAsString().contains(name))
                .andExpect(mvcResult -> mvcResult.getResponse().getContentAsString().contains(phoneNumber));
    }

    public String getMain() throws Exception {
        return this
                .mockMvc
                .perform(get(address))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
    }


    public List<String> regex(String s, String stringPattern) {
        Pattern pattern = Pattern.compile(stringPattern);
        Matcher matcher = pattern.matcher(s);

        List<String> list = new ArrayList<>();
        while (matcher.find()) {
            list.add(matcher.group(1));
        }
        return list;
    }


    private int checkIfContainsNumberOfRecord(String responce, String stringPattern, List<String> compareList) {
        List<String> list = regex(responce, stringPattern);
        list.contains(compareList.get(0));
        list.contains(compareList.get(1));
        list.contains(compareList.get(2));
        assertEquals(list.size(), compareList.size());
        if (list.size() == compareList.size()) {
            return list.size();
        }
        return -1;
    }


    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    private int geteRecordSList(String responce) {
        String stringPattern = "<span>(.+)?</span>";
        int size = checkIfContainsNumberOfRecord(responce, stringPattern, listOfNames);

        stringPattern = "<i>(.+)?</i>";
        checkIfContainsNumberOfRecord(responce, stringPattern, listOfphoneNumber);
        return size;
    }
}
