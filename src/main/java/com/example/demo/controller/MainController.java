package com.example.demo.controller;

import com.example.demo.domain.People;
import com.example.demo.repos.PeopleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@Controller
public class MainController {

    @Autowired
    private PeopleRepo peopleRepo;

    @GetMapping()
    public String main(@RequestParam(required = false, defaultValue = "") String name, Map<String, Object> model) {
        List<People> peoples;
        if (name != null && !name.isEmpty()) {
            peoples = peopleRepo.findByName(name);
        } else {
            peoples = (List<People>) peopleRepo.findAll();
        }

        model.put("peoples", peoples);

        return "main";
    }

    @PostMapping("addrecord")
    public String addrecord(@RequestParam String name, @RequestParam String phoneNumber, Map<String, Object> model){

        People people = new People(name, phoneNumber);
        peopleRepo.save(people);
        Iterable<People> peoples = peopleRepo.findAll();

        model.put("peoples", peoples);
        return "main";
    }

    @PostMapping("delete")
    public String deleteRecord(@RequestParam String id, Map<String, Object> model) {
        peopleRepo.deleteById(Long.valueOf(id));
        Iterable<People> peoples = peopleRepo.findAll();
        model.put("peoples", peoples);
        return "main";
    }

    @PostMapping("filter")
    public String filter(@RequestParam(required = false, defaultValue = "") String filter, Map<String, Object> model) {
        Iterable<People> byName;/* = peopleRepo.findByName(name);*/
        if (filter != null && !filter.isEmpty()) {
            byName = peopleRepo.findByName(filter);
        } else {
            byName = peopleRepo.findAll();
        }

        model.put("peoples", byName);
        return "main";
    }
}
