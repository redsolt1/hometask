package com.example.demo.repos;

import com.example.demo.domain.People;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface PeopleRepo extends CrudRepository<People, Long> {

//    List<People> findByName(String name);

    List<People> findByName(String name);
    Optional<People> findById(String id);

}
